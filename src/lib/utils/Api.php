<?php
    namespace dtw\utils;

    class Api {
        private $hooks = array();
        private $requiresKey = array();

        public function __construct() {

        }

        public function registerStaticHooks() {
            \dtw\Users::registerHooks();
            \dtw\Articles::registerHooks();
            \dtw\Discussions::registerHooks();
            \dtw\Playground::registerHooks();
            \dtw\utils\Markdown::registerHooks();
            \dtw\utils\Videos::registerHooks();
            \dtw\Images::registerHooks();
            \dtw\Medals::registerHooks();
        }

        public function registerHook($module, $method, $hook, $requiresKey = false) {
            if (!array_key_exists($module, $this->hooks)) {
                $this->hooks[$module] = array($method => $hook);
            } else {
                $this->hooks[$module][$method] = $hook;
            }

            if ($requiresKey) {
                if (!array_key_exists($module, $this->requiresKey)) {
                    $this->requiresKey[$module] = array($method);
                } else {
                    array_push($this->requiresKey[$module], $hook);
                }
            }
        }

        public function call($module, $method, $data) {
            if (!array_key_exists($module, $this->hooks)) {
                return $this->buildResponse(false, (object) [
                    'msg' => 'Unsupported module',
                    'code' => 0.1
                ]);
            }
            if (!array_key_exists($method, $this->hooks[$module])) {
                return $this->buildResponse(false, (object) [
                    'msg' => 'Unsupported method',
                    'code' => 0.2
                ]);
            }

            // Check if requires key
            if (array_key_exists($module, $this->requiresKey) &&
                in_array($method, $this->requiresKey[$module])) {
                    if (!isset($data['key']) || !$data['key']) {
                        return $this->buildResponse(false, (object) [
                            'msg' => 'API key required',
                            'code' => 0.6
                        ]);
                    }

                    if (!self::isValidKey($data['key'])) {
                        return $this->buildResponse(false, (object) [
                            'msg' => 'Invalid API key',
                            'code' => 0.7
                        ]);
                    }
            }

            $hook = $this->hooks[$module][$method];
            if (is_callable($hook)) {
                try {
                    $response = call_user_func($hook, $data);
                } catch (\Exception $e) {
                    return $this->buildResponse(false, (object) [
                        'msg' => $e->getMessage(),
                        'code' => 0.5
                    ]);
                }

                if (!isset($response) || $response === false) {
                    return $this->buildResponse(false, (object) [
                        'msg' => 'Error making request',
                        'code' => 0.4
                    ]);    
                }

                return $this->buildResponse(true, $response);
            } else {
                return $this->buildResponse(false, (object) [
                    'msg' => 'Error making request',
                    'code' => 0.3
                ]);
            }
        }

        private function buildResponse($status, $data) {
            $response = new \stdClass();

            // Return data
            if (!empty($data) && $status) {
                $response->data = $data;
            }
            
            // Return error
            if (!empty($data) && !$status) {
                $response->error = $data;
            }

            // Set correct response status
            $response->status = $status ? 'OK' : 'error';
            return $response;
        }

        public static function isValidKey($key) {
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE user_api_keys
                SET accessed = now(), hits = hits + 1
                WHERE `key` = :key
                LIMIT 1
            ');
            $stmt->execute(array(
                ':key' => $key
            ));

            return !!$stmt->rowCount();
        }

        public static function generateUserKey($name) {
            if (!\dtw\DtW::getInstance()->user->isAuth()) throw new \Exception('User must be logged in to generate key');

            if (!$name || !strlen($name)) {
                throw new \Exception('Key requires a name');
            }

            $key = Utils::generateToken(32);

            $stmt = \dtw\DtW::$db->prepare('INSERT INTO user_api_keys (`user_id`, `name`, `key`) VALUES (:id, :name, :key)');
            $stmt->execute(array(
                ':id' => \dtw\DtW::getInstance()->user->id,
                ':name' => $name,
                ':key' => $key
            ));

            return $key;
        }

        public static function getUserKeys() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT *
                FROM user_api_keys
                WHERE `user_id` = :id
                ORDER BY `created` DESC
            ');
            $stmt->execute(array(
                ':id' => \dtw\DtW::getInstance()->user->id
            ));

            return $stmt->fetchAll();
        }
    }
?>