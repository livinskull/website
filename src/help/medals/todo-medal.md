Current tasks to complete before being award the to-do medal:

* Set the status or bio on your profile
* Complete a level
* Read an article
* View our code of conduct
* Post a message on a discussion - be careful, spam posts won't count