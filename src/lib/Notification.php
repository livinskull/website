<?php
    namespace dtw;

    class Notification {
        private $_notification;

        public function __construct($data = null) {
            $DtW = \dtw\DtW::getInstance();

            $this->_notification = new \stdClass();
            if ($data) {
                $this->parse($data);
            }
        }

        public function __set($name, $value) {
            $this->_notification->$name = $value;
        }


        public function __isset($name) {
            return property_exists($this->_notification, $name);
        }


        public function __get($name) {
            return $this->_notification->$name;
        }

        public function markAsRead() {
            $DtW = \dtw\DtW::getInstance();

            // Mark notifications as read in DB
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE user_notifications
                SET `read` = now()
                WHERE `to` = :userID AND `read` IS NULL AND `notification_id` = :notificationID
                ORDER BY `created` DESC
            ');
            $stmt->execute(array(
                ':userID' =>  $DtW->user->id,
                ':notificationID' =>  $this->id
            ));
        }

        public function delete() {
            // Check user is the recipient
        }

        public function save($email = false) {
            $DtW = \dtw\DtW::getInstance();

            // Check user is the recipient
            if (!$this->_notification->to || !ctype_digit($this->_notification->to)) {
                throw new \Exception('Invalid to');
            }

            if (!$this->_notification->type) {
                throw new \Exception('Invalid type');
            }

            // Check user allows this notification type
            $allowed = \dtw\user\Settings::getUserSetting($this->_notification->to, 'notifications.' . $this->_notification->type);
            if ($allowed === false) {
                return;
                // throw new \Exception('User has declined to recieve these notifications');
            }

            if (!$this->_notification->from) {
                $this->_notification->from = $DtW->user->id;
            }

            if ($this->_notification->type !== 'message.new') {
                // Insert into DB
                $stmt = \dtw\DtW::$db->prepare('
                    INSERT INTO user_notifications (`to`, `from`, `type`, `data`)
                    VALUES (:to, :from, :type, :data);
                ');
                $stmt->execute(array(
                    ':to' => $this->_notification->to,
                    ':from' => $this->_notification->from,
                    ':type' => $this->_notification->type,
                    ':data' => json_encode($this->_notification->data)
                ));

                $this->_notification->id = \dtw\DtW::$db->lastInsertId();
                $this->_notification->created = date('c');

                // Insert into Redis incase user is logged in
                $key = 'user:' . $this->_notification->to . ':notifications';
                \dtw\DtW::$redis->sAdd($key, json_encode($this->_notification));
                \dtw\DtW::$redis->expire($key, 900);
            }

            // Queue email
            $emails = \dtw\user\Settings::getUserSetting($this->_notification->to, 'emails.notifications');
            if ($emails || $email === true) {
                $this->parse($this->_notification);
                $message = $this->_notification->message;

                if ($message->email) {
                    \dtw\utils\Emailer::send($this->_notification->to, $message->email->subject, 'notification', $message->email);
                }
            }
        }

        private function parse($data) {
            $DtW = \dtw\DtW::getInstance();

            $this->_notification->id = $data->id;

            try {
                $this->_notification->from = \dtw\Users::getUser($data->from);
            } catch (\Exception $e) {
                // User doesn't exist
            }

            $this->_notification->unread = !$data->read;

            $this->_notification->created = strtotime($data->created);

            if (!$this->_notification->message) {
                try {
                    $this->_notification->message = $this->getMessage($data->type, $data->data);
                } catch (\Exception $e) {
                    // Piece missing
                }
            }
        }

        private function getMessage($type, $data) {
            switch ($type) {
                case 'discussion.reply': $message = $this->getMessageDiscussionReply($data); break;
                case 'discussion.mention': $message = $this->getMessageDiscussionMention($data); break;
                case 'discussion.flag': $message = $this->getMessageDiscussionFlag($data); break;
                case 'discussion.moderation': $message = $this->getMessageDiscussionModeration($data); break;
                case 'article.moderation': $message = $this->getMessageArticleModeration($data); break;
                case 'medal.awarded': $message = $this->getMessageMedalAwarded($data); break;
                case 'ticket.reply': $message = $this->getMessageTicketReply($data); break;
                case 'ticket.status': $message = $this->getMessageTicketStatus($data); break;
                case 'message.new': $message = $this->getMessageMessageNew($data); break;
                case 'profile.follow': $message = $this->getMessageProfileFollow($data); break;
                default: $message = 'Unknown notification';
            }

            return $message;
        }

        private function getMessageDiscussionReply($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }
            
            // Get thread
            $DtW = \dtw\DtW::getInstance();
            $DtW->load('Discussions');
            try {
                $thread = $DtW->discussions->getThread($data->thread);
                $post = $DtW->discussions->getPost($thread->ID, $data->post);
            } catch (\Exception $e) {
                $messages = new \stdClass();
                $message = "%s posted a new mesage in a deleted thread";
                $messages->full = sprintf($message, $this->_notification->from->getLink());
                $messages->short =  "Reply in [deleted]";
                return $messages;
            }

            $url = $thread->permalink . '?notification=' . $this->_notification->id . '#post-' . $post->id;

            $messages = new \stdClass();
            $message = "%s posted a new mesage in <a href='%s'>%s</a>";
            $messages->full = sprintf($message, $this->_notification->from->getLink(), $url, $thread->title);
            $message = "Reply in <a href='%s'>%s</a>";
            $messages->short = sprintf($message, $url, $thread->title);

            $messages->email = new \stdClass();
            $message = "%s posted a new mesage in \"%s\"";
            $messages->email->subject = sprintf($message, $this->_notification->from->username, htmlspecialchars($thread->title));
            $message = "%s posted a new mesage in <a href='%s'>%s</a>";
            $messages->email->message = sprintf($message, $this->_notification->from->getLink(), $url, htmlspecialchars($thread->title));
            $messages->email->excerpt = nl2br($post->message->safe);

            return $messages;
        }

        private function getMessageDiscussionMention($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }
            
            // Get thread
            $DtW = \dtw\DtW::getInstance();
            $DtW->load('Discussions');
            try {
                $thread = $DtW->discussions->getThread($data->thread);
                $post = $DtW->discussions->getPost($thread->ID, $data->post);
            } catch (\Exception $e) {
                $messages = new \stdClass();
                $message = "%s mentioned you in a deleted thread";
                $messages->full = sprintf($message, $this->_notification->from->getLink());
                $messages->short =  "Mentioned in [deleted]";
                return $messages;
            }

            $url = $thread->permalink . '?notification=' . $this->_notification->id . '#post-' . $post->id;

            $messages = new \stdClass();
            $message = "%s mentioned you in <a href='%s'>%s</a>";
            $messages->full = sprintf($message, $this->_notification->from->getLink(), $url, $thread->title);
            $message = "Mentioned in <a href='%s'>%s</a>";
            $messages->short = sprintf($message, $url, $thread->title);

            $messages->email = new \stdClass();
            $message = "%s mentioned you in \"%s\"";
            $messages->email->subject = sprintf($message, $this->_notification->from->username, htmlspecialchars($thread->title));
            $message = "%s mentioned you in <a href='%s'>%s</a>";
            $messages->email->message = sprintf($message, $this->_notification->from->getLink(), $url, htmlspecialchars($thread->title));
            $messages->email->excerpt = nl2br($post->message->safe);

            return $messages;
        }

        private function getMessageDiscussionFlag($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }

            // Get thread
            $DtW = \dtw\DtW::getInstance();
            $DtW->load('Discussions');
            try {
                $thread = $DtW->discussions->getThread($data->thread);
            } catch (\Exception $e) {
                $messages = new \stdClass();
                $message = "A flag was %s";
                $messages->full = sprintf($message, $data->status);
                $message = "Flag %s";
                $messages->short = sprintf($message, $data->status);
                return $messages;
            }

            $messages = new \stdClass();
            $message = "A flag in <a href='%s'>%s</a> was %s";
            $messages->full = sprintf($message, $url, $article->title, $data->status);
            $message = "Flag %s";
            $messages->short = sprintf($message, $data->status);

            return $messages;
        }

        private function getMessageDiscussionModeration($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }
            
            // Get thread
            $DtW = \dtw\DtW::getInstance();
            $DtW->load('Discussions');
            try {
                $thread = $DtW->discussions->getThread($data->thread);
            } catch (\Exception $e) {
                //
            }

            $url = $thread->permalink . '?notification=' . $this->_notification->id;

            $messages = new \stdClass();
            $message = "Your post in <a href='%s'>%s</a> was deleted";
            $messages->full = sprintf($message, $url, $thread->title);
            $message = "<a href='%s'>Post</a> deleted";
            $messages->short = sprintf($message, $thread->title);

            return $messages;
        }

        private function getMessageArticleModeration($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }
            
            // Get thread
            $article = \dtw\Articles::getArticle($data->article);

            $url = $article->permalink . '?notification=' . $this->_notification->id;

            $messages = new \stdClass();
            $message = "Your article <a href='%s'>%s</a> was %s";
            $messages->full = sprintf($message, $url, $article->title, $data->status);
            $message = "<a href='%s'>%s</a> %s";
            $messages->short = sprintf($message, $url, $article->title, $data->status);

            $messages->email = new \stdClass();
            $message = "Your article %s was %s";
            $messages->email->subject = sprintf($message, $article->title, $data->status);
            $message = "Your article <a href='%s'>%s</a> was %s";
            $messages->email->message = sprintf($message, $url, htmlspecialchars($article->title), $data->status);

            return $messages;
        }

        private function getMessageMedalAwarded($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }
            
            // Get thread
            $medal = Medals::getMedal($data->medal);

            $url = \dtw\DtW::$config->get('site.domain') . '/help/medals?notification=' . $this->_notification->id . '#' . $medal->label;

            $messages = new \stdClass();
            $message = "You have been awarded the <i class='fas fa-medal c-%s'></i> <a href='%s'>%s</a> medal";
            $messages->full = sprintf($message, $medal->colour, $url, $medal->label);
            $message = "Awarded <i class='fas fa-medal c-%s'></i> <a href='%s'>%s</a>";
            $messages->short = sprintf($message, $medal->colour, $url, $medal->label);

            return $messages;
        }

        private function getMessageTicketReply($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }

            $url = \dtw\DtW::$config->get('site.domain') . '/help/contact/' . $data->ticket . '?notification=' . $this->_notification->id;

            $messages = new \stdClass();
            $message = "A reply has been added to <a href='%s'>Ticket #%d</a>";
            $messages->full = sprintf($message, $url, $data->ticket);
            $message = "Reply to <a href='%s'>Ticket #%d</a>";
            $messages->short = sprintf($message, $url, $data->ticket);

            $messages->email = new \stdClass();
            $messages->email->subject = "A reply has been added to a ticket";
            $messages->email->message = $messages->full;

            return $messages;
        }

        private function getMessageTicketStatus($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }

            $url = \dtw\DtW::$config->get('site.domain') . '/help/contact/' . $data->ticket . '?notification=' . $this->_notification->id;

            $messages = new \stdClass();
            $message = "<a href='%s'>Ticket #%d</a> marked as %s";
            $messages->full = sprintf($message, $url, $data->ticket, $data->status);
            $message = "<a href='%s'>Ticket #%d</a> %s";
            $messages->short = sprintf($message, $url, $data->ticket, $data->status);

            $messages->email = new \stdClass();
            $message = "Your ticket has been marked as %s";
            $messages->email->subject = sprintf($message, $data->status);
            $messages->email->message = $messages->full;

            return $messages;
        }

        private function getMessageMessageNew($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            if (!is_object($data)) {
                $data = json_decode($data);
            }

            $url = \dtw\DtW::$config->get('site.domain') . '/message/' . $data->conversation . '?notification=' . $this->_notification->id;

            $messages = new \stdClass();
            $message = "%s sent you a new <a href='%s'>personal message</a>";
            $messages->full = sprintf($message, $this->_notification->from->getLink(), $url);
            $message = "New <a href='%s'>PM</a> from %s";
            $messages->short = sprintf($message, $url, $this->_notification->from->getLink());

            $messages->email = new \stdClass();
            $message = "%s sent you a new private message";
            $messages->email->subject = sprintf($message, $this->_notification->from->username, $thread->title);
            $messages->email->message = $messages->full;

            return $messages;
        }

        private function getMessageProfileFollow($data) {
            if (!$data) {
                $message = 'Unknown notification';
            }

            $link = $this->_notification->from->getLink(null, '?notification=' . $this->_notification->id);

            $messages = new \stdClass();
            $message = "%s started following your profile";
            $messages->full = sprintf($message, $link);
            $message = "%s followed you";
            $messages->short = sprintf($message, $link);

            return $messages;
        }

    }