<?php
    namespace dtw;

    class Discussions {
        private $pages;

        public function __construct() {
            $this->pages = new \stdClass();

            $threads = \dtw\DtW::$redis->get('discussions');
            if ($threads) {
                $this->pages = json_decode($threads);
            }

            $topics = \dtw\DtW::$redis->get('discussion:topics');
            if ($topics) {
                $this->topics = json_decode($topics);
            } else {
                $this->topics = $this->loadTopics();
                \dtw\DtW::$redis->set('discussion:topics', json_encode($this->topics));
            }
        }

        public static function registerHooks() {
            \dtw\DtW::$api->registerHook('discussions', 'view', function($params) {
                if (!isset($_POST['id'])) {
                    throw new \Exception('Invalid ID');
                }

                $discussions = new self();
                $discussions->getThread($_POST['id'])->countView();

                return true;
            });

            \dtw\DtW::$api->registerHook('discussions', 'awardedKarma', function($params) {
                if (!isset($_POST['thread'])) {
                    throw new \Exception('Invalid thread');
                }

                $discussions = new self();
                return $discussions->getThread($_POST['thread'])->awardedKarma();
            });

            // External API
            \dtw\DtW::$api->registerHook('discussions', 'threads', function($params) {
                return self::apiThreads($params);
            }, true);
        }

        public function getList($page = 1, $topic = 'all', $order = 'popular') {
            // Handle API requests
            if (is_array($page) && isset($page['page'])) {
                $page = $page['page'];
            }

            if (!$page) {
                $page = 1;
            }

            if (is_object($topic)) {
                $topic_id = $topic->topic_id;
                $topic_slug = $topic->slug;
            } else {
                $topic_slug = $topic;
            }

            if (!isset($this->pages->{$topic_slug})) {
                $this->pages->{$topic_slug} = new \stdClass();
            }

            if (!isset($this->pages->{$topic_slug}->{$order})) {
                $this->pages->{$topic_slug}->{$order} = new \stdClass();
            }

            if (isset($this->pages->{$topic_slug}->{$order}->{$page}) && is_array($this->pages->{$topic_slug}->{$order}->{$page})) {
                return $this->pages->{$topic_slug}->{$order}->{$page};
            }

            if (is_numeric($page)) {
                $count = 10;
                $offset = ($page * $count) - $count;
            } else if ($page !== 'sticky') {
                throw new \Exception('Invalid request');
            } else {
                $offset = 0;
                $count = 25;
            }

            try {
                switch ($order) {
                    case 'latest': $sql = $this->getLatestSQL($topic_id); break;
                    case 'top': $sql = $this->getMostRepliesSQL($topic_id); break;
                    case 'popular': $sql = $this->getPopularSQL($topic_id); break;
                    case 'no-replies': $sql = $this->getNoRepliesSQL($topic_id); break;
                    default: throw new \Exception('Invalid request');
                }
            } catch (Exception $e) {
                throw $e;
            }

            $stmt = \dtw\DtW::$db->prepare($sql);

            if ($page === 'sticky') {
                $stmt->bindValue(':sticky', 1, \PDO::PARAM_INT);
            } else if (isset($offset)) {
                $stmt->bindValue(':sticky', 0, \PDO::PARAM_INT);
            } else {
                throw new \Exception('Invalid request');
            }

            if ($topic_id) {
                $stmt->bindValue(':topic_id', $topic_id); 
            }

            $stmt->bindValue(':offset', (int) $offset, \PDO::PARAM_INT); 
            $stmt->bindValue(':count', (int) $count, \PDO::PARAM_INT);

            $stmt->execute();

            $threads = array();
            if ($stmt->rowCount()) {
                $threads = $stmt->fetchAll(\PDO::FETCH_COLUMN);

                // foreach($threads AS &$thread) {
                //     $t = new discussion\Thread($thread->thread_id);
                //     $thread = $t->getData();
                // }
            } else {
                // throw new \Exception('No threads found');
            }

            // Store for later use
            $this->pages->{$topic_slug}->{$order}->{$page} = $threads;

            \dtw\DtW::$redis->set('discussions', json_encode($this->pages));

            return $threads;
        }

        private function getLatestSQL($topic) {
            return sprintf('
                SELECT `forum_threads`.`thread_id`
                FROM `forum_threads`
                JOIN (SELECT MAX(`posted`) AS `latest`, `thread_id` FROM `forum_thread_posts` WHERE `deleted` = 0 GROUP BY `thread_id`) `posts`
                ON `posts`.thread_id = `forum_threads`.thread_id
                LEFT JOIN `forum_topics`
                ON `forum_topics`.`topic_id` = `forum_threads`.`topic_id`
                LEFT JOIN `forum_topics` AS `topic_parent`
                ON `topic_parent`.`topic_id` = `forum_topics`.`parent`
                WHERE `deleted` = 0 AND `sticky` = :sticky %s
                ORDER BY `posts`.`latest` DESC
                LIMIT :offset, :count
            ', $topic ? 'AND (`forum_threads`.`topic_id` = :topic_id OR `topic_parent`.`topic_id` = :topic_id)' : null);
        }

        private function getPopularSQL($topic) {
            return sprintf('
                SELECT `forum_threads`.`thread_id`, (COALESCE(`day`.`score`, 0) + COALESCE(`week`.`score`, 0) + COALESCE(`month`.`score`, 0) +  `posts`.`score`) AS `totalscore`
                FROM `forum_threads`
                JOIN (SELECT COUNT(*) AS `score`, MAX(`posted`) AS `latest`, `thread_id` FROM `forum_thread_posts` WHERE `deleted` = 0 GROUP BY `thread_id`) `posts`
                ON `posts`.thread_id = `forum_threads`.thread_id
                LEFT JOIN (SELECT COUNT(*) * 1000 AS `score`, `thread_id` FROM `forum_thread_posts` WHERE `posted` > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND `deleted` = 0 GROUP BY `thread_id`) `day`
                ON `day`.thread_id = `forum_threads`.thread_id
                LEFT JOIN (SELECT COUNT(*) * 250 AS `score`, `thread_id` FROM `forum_thread_posts` WHERE `posted` > DATE_SUB(NOW(), INTERVAL 7 DAY) AND `deleted` = 0 GROUP BY `thread_id`) `week`
                ON `week`.thread_id = `forum_threads`.thread_id
                LEFT JOIN (SELECT COUNT(*) * 100 AS `score`, `thread_id` FROM `forum_thread_posts` WHERE `posted` > DATE_SUB(NOW(), INTERVAL 1 MONTH) AND `deleted` = 0 GROUP BY `thread_id`) `month`
                ON `month`.thread_id = `forum_threads`.thread_id
                LEFT JOIN `forum_topics`
                ON `forum_topics`.`topic_id` = `forum_threads`.`topic_id`
                LEFT JOIN `forum_topics` AS `topic_parent`
                ON `topic_parent`.`topic_id` = `forum_topics`.`parent`
                WHERE `deleted` = 0 AND `sticky` = :sticky %s
                ORDER BY `totalscore` DESC, `posts`.`latest` DESC
                LIMIT :offset, :count
            ', $topic ? 'AND (`forum_threads`.`topic_id` = :topic_id OR `topic_parent`.`topic_id` = :topic_id)' : null);
        }

        private function getMostRepliesSQL($topic) {
            return sprintf('
                SELECT `forum_threads`.`thread_id`
                FROM `forum_threads`
                JOIN (SELECT MAX(`posted`) AS `latest`, COUNT(*) - 1 AS `replies`, `thread_id` FROM `forum_thread_posts` WHERE `deleted` = 0 GROUP BY `thread_id`) `posts`
                ON `posts`.thread_id = `forum_threads`.thread_id
                LEFT JOIN `forum_topics`
                ON `forum_topics`.`topic_id` = `forum_threads`.`topic_id`
                LEFT JOIN `forum_topics` AS `topic_parent`
                ON `topic_parent`.`topic_id` = `forum_topics`.`parent`
                WHERE `deleted` = 0 AND `sticky` = :sticky %s
                ORDER BY `posts`.`replies` DESC, `posts`.`latest` DESC
                LIMIT :offset, :count
            ', $topic ? 'AND (`forum_threads`.`topic_id` = :topic_id OR `topic_parent`.`topic_id` = :topic_id)' : null);
        }

        private function getNoRepliesSQL($topic) {
            return sprintf('
                SELECT `forum_threads`.`thread_id`
                FROM `forum_threads`
                JOIN (SELECT MAX(`posted`) AS `latest`, COUNT(*) - 1 AS `replies`, `thread_id` FROM `forum_thread_posts` WHERE `deleted` = 0 GROUP BY `thread_id`) `posts`
                ON `posts`.thread_id = `forum_threads`.thread_id
                LEFT JOIN `forum_topics`
                ON `forum_topics`.`topic_id` = `forum_threads`.`topic_id`
                LEFT JOIN `forum_topics` AS `topic_parent`
                ON `topic_parent`.`topic_id` = `forum_topics`.`parent`
                WHERE `deleted` = 0 AND `sticky` = :sticky AND `posts`.`replies` = 0 %s
                ORDER BY `posts`.`latest` DESC
                LIMIT :offset, :count
            ', $topic ? 'AND (`forum_threads`.`topic_id` = :topic_id OR `topic_parent`.`topic_id` = :topic_id)' : null);
        }

        public function getThread($thread) {
            return new \dtw\discussion\Thread($thread);
        }

        public static function getThreads($args = array()) {
            $params = array();
            $sql = 'SELECT `thread_id` AS `id`, `title` FROM forum_threads WHERE `deleted` = 0';

            if ($args['q']) {
                $sql .= ' AND `title` LIKE :q';
                $params[':q'] = "{$args['q']}";
            }

            if ($args['level_id']) {
                $sql .= ' AND `level_id` LIKE :id';
                $params[':id'] = "{$args['level_id']}";
            }

            $orderByAllows = array('views');
            if ($args['orderBy'] && in_array(strtolower($args['orderBy']), $orderByAllows)) {
                $order = strtolower($args['order']) == 'asc' ? 'ASC' : 'DESC';
                $orderBy = $args['orderBy'];
                $sql .= " ORDER BY `{$orderBy}` {$order}";
            }

            if ($args['limit']) {
                $limit = intval($args['limit']);
                $sql .= " LIMIT {$limit}";
            } else {
                $sql .= " LIMIT 1000";
            }

            $stmt = \dtw\DtW::$db->prepare($sql); 
            $stmt->execute($params); 
            if ($stmt->rowCount()) {
                $threads = $stmt->fetchAll();

                return $threads;
            }
        }

        public function createThread($data) {
            // Create thread
            $thread = new \dtw\discussion\Thread();
            $thread->create($data);

            // Add first post
            $post = new \dtw\discussion\Post();
            $post->create($thread->id, $data);

            // Clear cache
            $thread->clearCache();

            // Update search
            $thread->buildSearch();

            // Notify users
            $mentions = \dtw\utils\Markdown::parseMentions($data['message']);
            if (count($mentions)) {
                $DtW = \dtw\DtW::getInstance();
                $notificationData = (object) array(
                    'thread' => $thread->id,
                    'post' => $post->id
                );
                foreach($mentions AS $mention) {
                    if ($mention->id === $DtW->user->id) {
                        continue;
                    }

                    \dtw\Notifications::send($mention->id, 'discussion.mention', $notificationData);
                }
            }

            return $thread->permalink;
        }

        public function report($permalink, $data) {
            if (!isset($data['post_id']) && !isset($data['reason'])) {
                throw new \Excpetion('Missing fields');
            }

            $thread = new \dtw\discussion\Thread($permalink);

            if (!$thread) {
                throw new \Exception('Discussion not found');
            }

            if (!in_array($data['post_id'], $thread->posts)) {
                throw new \Exception('Error in request');
            }

            $reasons = array('topic','spam','quality','other');
            if (!in_array($data['reason'], $reasons)) {
                throw new \Exception('Invalid reason');
            }

            $DtW = \dtw\DtW::getInstance();
            $stmt = \dtw\DtW::$db->prepare("
                INSERT INTO `forum_flags` (`post_id`, `user_id`, `type`, `comment`)
                VALUES (:post_id, :user_id, :type, :comment);
            ");

            try {
                $stmt->execute(array(
                    ':post_id' => $data['post_id'],
                    ':user_id' => $DtW->user->id,
                    ':type' => $data['reason'],
                    ':comment' => $data['comment']
                ));
            } catch (\Exception $e) {
                throw new \Exception('Report not submitted. You may have already reported this post.');
            }
        }

        private function loadTopics() {
            $stmt = \dtw\DtW::$db->prepare('SELECT `topic_id`, `title`, `slug`, `description` FROM `forum_topics` WHERE `parent` IS NULL ORDER BY `order` ASC, `title` ASC'); 
            $stmt->execute();
            $topics = $stmt->fetchAll();

            foreach($topics AS &$topic) {
                $stmt = \dtw\DtW::$db->prepare('SELECT `topic_id`, `title`, `slug`, `description`, `parent`  FROM `forum_topics` WHERE `parent` = :topic_id ORDER BY `order` ASC, `title` ASC'); 
                $stmt->execute(array('topic_id' => $topic->topic_id));
                $children = $stmt->fetchAll();

                if ($children && count($children)) {
                    $topic->children = $children;
                }
            }

            return $topics;
        }

        public function getTopic($lookup) {
            if (!count($this->topics)) {
                throw new \Exception('No topics loaded');
            }

            if (ctype_digit(strval($lookup))) {
                $topic = $this->getTopicById($lookup);
            } else {
                $topic = $this->getTopicBySlug($lookup);
            }

            if (!$topic) {
                throw new \Exception('Topic doesn\'t exist');
            }

            return clone $topic;
        }

        public function getTopics() {
            return $this->topics;
        }

        private function getTopicBySlug($slug) {
            foreach($this->topics AS $topic) {
                if ($topic->slug === $slug) {
                    return $topic;
                }

                if (isset($topic->children) && count($topic->children)) {
                    foreach($topic->children AS $child) {
                        if ($child->slug === $slug) {
                            return $child;
                        }
                    }
                }
            }
        }

        private function getTopicById($id) {
            foreach($this->topics AS $topic) {
                if ($topic->topic_id === $id) {
                    return $topic;
                }

                if (isset($topic->children) && count($topic->children)) {
                    foreach($topic->children AS $child) {
                        if ($child->topic_id === $id) {
                            return $child;
                        }
                    }
                }
            }

            throw new \Exception('Topic doesn\'t exist');
        } 

        public function checkRecentPosts() {
            if (!defined('CRON')) {
                throw new \Exception('Method not run from CRON');
            }

            $stmt = \dtw\DtW::$db->prepare('SELECT `post_id` FROM `forum_thread_posts` WHERE `deleted` = 0 AND `posted` > NOW() - INTERVAL 5 MINUTE'); 
            $stmt->execute();
            $posts = $stmt->fetchAll();
            
            foreach($posts AS $p) {
                $post = new \dtw\discussion\Post($p->post_id);
                $post->checkPost();
            }
        }

        public function saveViewCounts() {
            if (!defined('CRON')) {
                throw new \Exception('Method not run from CRON');
            }

            // Get all thread IDs in redis
            $key = 'discussions:updated';

            do {
                $updated = \dtw\DtW::$redis->sPop($key);
                if ($updated) {
                    $thread = $this->getThread($updated);
                    $thread->storeViews();
                }
            } while($updated);
        }


        public static function getPost($threadId, $postId) {
            $DtW = \dtw\DtW::getInstance();

            $post = new \dtw\discussion\Post($postId);

            return $post;
        }


        public function getPostToEdit($threadId, $postId) {
            $post = self::getPost($threadId, $postId);

            if (!$post->isEditable) {
                throw new \Exception('Invalid request');
            }

            return $post;
        }

        public function getStats() {
            $key = 'discussions:stats';
            $stats = \dtw\DtW::$redis->get($key);

            if ($stats) {
                $stats = json_decode($stats);
            } else {
                $stats = array();

                $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `threads` FROM `forum_threads` WHERE `deleted` = 0'); 
                $stmt->execute();
                $row = $stmt->fetch();
                $stats['threads'] = $row->threads;

                $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `posts` FROM `forum_thread_posts` WHERE `deleted` = 0'); 
                $stmt->execute();
                $row = $stmt->fetch();
                $stats['replies'] = $row->posts - $stats['threads'];

                $stmt = \dtw\DtW::$db->prepare('SELECT distinct(`author`) AS `voices` FROM `forum_thread_posts` WHERE `deleted` = 0'); 
                $stmt->execute();
                $stats['voices'] = $stmt->rowCount();

                \dtw\DtW::$redis->set($key, json_encode($stats));
                \dtw\DtW::$redis->expire($key, 900);
            }

            return $stats;
        }

        public static function apiThreads($params) {
            $DtW = \dtw\DtW::getInstance();
            $DtW->load('Discussions');

            $order = 'latest';
            if (in_array($params['order'], array('latest', 'top', 'popular', 'no-replies'))) {
                $order = $params['order'];
            }

            $items = $DtW->discussions->getList(1, 'all', $order);

            $items = array_map(function($id) use ($DtW) {
                $thread = $DtW->discussions->getThread($id);

                $data = $thread->getData();
                unset($data->posts);
                unset($data->topic->parent->children);

                return $data;
            }, $items);

            return $items;
        }
    }