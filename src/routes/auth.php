<?php
    $this->respond('GET', '/logout', function ($request, $response, $service, $app) {
        $app->DtW->user->logout();
        $response->redirect('/');
        $response->send();
        $this->skipRemaining();
    });

    // Don't allow logged in users to see login page
    $this->respond(function($request, $response, $service, $app) {
        if ($app->DtW->user->isAuth()) {
            //
        } else if (isset($_COOKIE['auth_remember'])) {
            // Check if their remember me token works
            if ($app->DtW->user->checkRememberMe()) {    
                $response->redirect('/dashboard');
                $response->send();
                $this->skipRemaining();
            }
        }
    });

    $this->respond('GET', '/[signup:signup]?', function ($request, $response, $service, $app) {
        if ($app->DtW->user->isAuth()) {
            $response->redirect('/dashboard');
            $response->send();
            $this->skipRemaining();
        }

        // Do they need to provide 2FA?
        if (isset($_SESSION['twoFactorUser'])) {
            $session = $_SESSION['twoFactorUser'];
            echo $app->DtW->tmpl->render('auth/2fa.twig', array( 'remember' => $session->remember, 'token' => \dtw\utils\CSRF::generate() ));
        } else {
            $tmpDetails = [];
            if (isset($_SESSION['auth.tmpDetails'])) {
                $tmpDetails =  $_SESSION['auth.tmpDetails'];
                unset($_SESSION['auth.tmpDetails']);
            }

            echo $app->DtW->tmpl->render('auth/index.twig', array( 'signup' => $request->signup, 'tmpDetails' => $tmpDetails, 'token' => \dtw\utils\CSRF::generate() ));
        }

        $response->send();
        $this->skipRemaining();
    });

    // Local authentication
    $this->respond('POST', '', function ($request, $response, $service, $app) {
        if ($app->DtW->user->isAuth()) {
            $response->redirect('/dashboard');
            $response->send();
            $this->skipRemaining();
        }

        $app->DtW->load('Auth');

        try {
            $app->DtW->auth->login('website');

            // User logged in
            if (isset($_SESSION['twoFactorUser'])) {
                $response->redirect('/auth');
            } else if (isset($_SESSION['auth.redirect'])) {
                $response->redirect($_SESSION['auth.redirect']);
            } else {
                $response->redirect('/dashboard');
            }
            unset($_SESSION['auth.redirect']);
        } catch (Exception $e) {
            $_SESSION['auth.tmpDetails'] = $_POST;

            \dtw\utils\Flash::add($e->getMessage(), 'error', 'auth.login');
            $response->redirect('/auth');
        }

        $response->send();
        $this->skipRemaining();
    });

    // 2FA
    $this->respond('POST', '/2fa', function ($request, $response, $service, $app) {
        if ($app->DtW->user->isAuth()) {
            $response->redirect('/dashboard');
            $response->send();
            $this->skipRemaining();
        }

        // Do they need to be here?
        if (!isset($_SESSION['twoFactorUser'])) {
            $response->redirect('/auth');
            $response->send();

            $this->skipRemaining();
        }

        if (!isset($_POST['twoFactor']) || empty($_POST['twoFactor'])) {
            dtw\utils\Flash::add('Enter a code to verify', 'error');
            $response->redirect('./');
            $response->send();

            $this->skipRemaining();
        }

        $code = $_POST['twoFactor'];
        $remember = isset($_POST['twoFactor-remember']);

        try {
            $app->DtW->user->verify2FA($code, $remember);

            $response->redirect('/dashboard');
            $response->send();
        } catch (Exception $e) {
            dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/auth');
            $response->send();

        }
        
        $this->skipRemaining();
    });

    // Local registration
    $this->respond('POST', '/signup', function ($request, $response, $service, $app) {
        if ($app->DtW->user->isAuth()) {
            $response->redirect('/dashboard');
            $response->send();
            $this->skipRemaining();
        }

        $app->DtW->load('Auth');

        try {
            $app->DtW->auth->register();

            // User logged in
            if (!empty($_SESSION['auth.redirect'])) {
                $response->redirect($_SESSION['auth.redirect']);
                unset($_SESSION['auth.redirect']);
            } else {
                $response->redirect('/dashboard');
            }
        } catch (Exception $e) {
            $_SESSION['auth.tmpDetails'] = $_POST;

            dtw\utils\Flash::add($e->getMessage(), 'error', 'auth.signup');
            $response->redirect('/auth/signup');
        }

        $response->send();
        $this->skipRemaining();
    });

    // Password request
    $this->respond('GET', '/forgot', function ($request, $response, $service, $app) {
        if (isset($_GET['userID']) && isset($_GET['token'])) {

            try {
                $user = new \dtw\user\User();
                $user->loginWithToken($_GET['userID'], $_GET['token']);

                if (isset($_SESSION['twoFactorUser'])) {
                    $response->redirect('/auth');
                } else {
                    $response->redirect('/settings/password');
                }
            } catch (Exception $e) {
                dtw\utils\Flash::add($e->getMessage(), 'error');
                $response->redirect('/auth/forgot');
            }

            $response->send();
            $this->skipRemaining();
        }

        echo $app->DtW->tmpl->render('auth/forgot.twig', array('token' => \dtw\utils\CSRF::generate()));

        $response->send();
        $this->skipRemaining();
    });

    // Password request
    $this->respond('POST', '/forgot', function ($request, $response, $service, $app) {
        try {
            \dtw\Auth::forgot($_POST['forgot']);
            \dtw\utils\Flash::add('Password reset email sent', 'success', 'auth.login');

            $response->redirect('/auth');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/auth/forgot');
        }

        $response->send();
        $this->skipRemaining();
    });


    // OAUTH

    use \OAuth\Common\Storage\Session;
    use \OAuth\Common\Consumer\Credentials;

    $this->respond('GET', '/setup', function ($request, $response, $service, $app) {
        if (isset($_SESSION['newUser'])) {
            echo $app->DtW->tmpl->render('auth/setup.twig', array('newUser' => $_SESSION['newUser'], 'token' => \dtw\utils\CSRF::generate()));

            $response->send();
            $this->skipRemaining();
        } else {
            $response->redirect('/auth');

            $response->send();
            $this->skipRemaining();
        }
    });

    $this->respond('POST', '/setup', function ($request, $response, $service, $app) {
        $details = $_SESSION['newUser'];

        if (isset($_SESSION['newUser'])) {
            $details['username'] = $_POST['username'];
            $details['email'] = $_POST['email'];

            try {
                $app->DtW->user->create($details);
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $app->DtW->tmpl->render('auth/setup.twig', array('newUser' => $details, 'error' => $error));

                $response->send();
                $this->skipRemaining();
            }
            
            if (!empty($_SESSION['auth.redirect'])) {
                $response->redirect($_SESSION['auth.redirect']);
                unset($_SESSION['auth.redirect']);
            } else {
                $response->redirect('/dashboard');
            }
        } else {
            $response->redirect('/auth');
        }

        $response->send();
        $this->skipRemaining();
    });

    $this->respond('GET', '/[:provider]', function ($request, $response, $service, $app) {
        $app->DtW->load('Auth');

        try {
            if (!$app->DtW->auth->login($request->provider, $response)) {
                $response->redirect('/auth');
            }
        } catch (Exception $e) {
            dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/auth');
        }

        $response->send();
        $this->skipRemaining();
    });