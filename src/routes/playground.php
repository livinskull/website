<?php
    $this->respond(function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
    }); 

    $this->respond('', function ($request, $response, $service, $app) {
        $args = array(
            'breadcrumb' => array(
                'Playground' => '/playground'
            )
        );

        try {
            $args['levels'] = \dtw\Playground::getLevelsOrdered();
            $args['subjects'] = \dtw\Playground::getLevelSubjects();

            if (isset($_GET['subject'])) {
                $slug = $_GET['subject'];
                if (array_key_exists($slug, $args['subjects'])) {
                    $subject = $args['subjects'][$slug];
                    $subject->slug = $slug;
                    $args['breadcrumb'][$subject->name] = '/playground?subject=' . $slug;
                    $args['subject'] = $subject;
                }
            }

            return $app->DtW->tmpl->render('playground/index.twig', $args);
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            die($e);
            $response->redirect('/dashboard');
        }
    });

    $this->get('/new', function ($request, $response, $service, $app) {
        if (!$app->DtW->user->hasPrivilege('playground.edit')) {
            $response->redirect('/playground');

            $response->send();
            $this->skipRemaining();
        }

        $breadcrumb = array(
            'Playground' => '/playground',
            'New level' => '/playground/new'
        );

        $form = \dtw\Playground::getLevelEditorForm();

        echo $app->DtW->tmpl->render('form.twig', array('breadcrumb' => $breadcrumb, 'form' => $form));
        $response->send();
        $this->skipRemaining();
    });

    $this->respond('POST', '/new', function ($request, $response, $service, $app) {
        if (!$app->DtW->user->hasPrivilege('playground.edit')) {
            $response->redirect('/playground');

            $response->send();
            $this->skipRemaining();
        }

        try {
            $level = \dtw\Playground::createLevel($_POST);

            \dtw\utils\Flash::add('Level created', 'success');

            $response->redirect('/playground/' . $level . '/edit');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect($request->pathname());
        }

        $response->send();
        $this->skipRemaining();

    });

    $this->respond('/[:levelSlug]', function ($request, $response, $service, $app) {
        try {
            $level = \dtw\Playground::getBySlug($request->levelSlug);
            $level->onLevel();

            // Admin functions
            if (count($_POST)) {
                $level->flash = $level->checkAnswer($_POST);
            } else if (count($_GET)) {
                $level->flash = $level->checkAnswer($_GET);
            }

            $breadcrumb = array(
                'Playground' => '/playground',
                $level->title => '/playground/' . $level->slug
            );

            return $app->DtW->tmpl->render('playground/level.twig', array('breadcrumb' => $breadcrumb, 'level' => $level));
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/playground');
        }
    });

    $this->respond('GET', '/[:levelSlug]/edit', function ($request, $response, $service, $app) {
        if (!$app->DtW->user->hasPrivilege('playground.edit')) {
            $response->redirect('/playground');

            $response->send();
            $this->skipRemaining();
        }

        try {
            $level = \dtw\Playground::getBySlug($request->levelSlug);

            $breadcrumb = array(
                'Playground' => '/playground',
                $level->title => '/playground/' . $level->slug
            );

            $form = \dtw\Playground::getLevelEditorForm($level);

            return $app->DtW->tmpl->render('form.twig', array('breadcrumb' => $breadcrumb, 'form' => $form, 'level' => $level));
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/playground');
        }
    });

    $this->respond('POST', '/[:levelSlug]/edit', function ($request, $response, $service, $app) {
        if (!$app->DtW->user->hasPrivilege('playground.edit')) {
            $response->redirect('/playground');

            $response->send();
            $this->skipRemaining();
        }

        try {
            $level = \dtw\Playground::getBySlug($request->levelSlug);

            // Save updates
            $slug = $level->updateLevel($_POST);

            \dtw\utils\Flash::add('Level updated', 'success');

            $response->redirect('/playground/' . $slug . '/edit');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect($request->pathname());
        }
        $response->send();
        $this->skipRemaining();

    });
