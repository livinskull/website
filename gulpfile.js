// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    spawn = require('child_process').spawn;

// Static files
gulp.task('static', function() {
    return gulp.src(['src/**/*.*', '!src/js/*.*', '!src/**/*.css', '!src/**/*.scss'])
        .pipe(gulp.dest('site'))
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src(['src/js/*.js', '!src/js/_*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Move CSS
gulp.task('css', function() {
    return gulp.src('src/**/*.css')
        .pipe(gulp.dest('site'));
});

// Compile our Sass
gulp.task('sass', function() {
    var s = sass();
    s.on('error', function(e) { console.log(e); console.log("Error processing SASS"); this.emit('end'); });

    return gulp.src('src/**/*.scss')
        .pipe(s)
        .pipe(gulp.dest('site'));
});

// Concatenate & Minify JS - unless filename starts with an underscore
gulp.task('scripts', function() {
    return gulp.src(['src/www/js/*.js', '!src/www/js/_*.js'])
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('site/www/js'));
});

gulp.task('scriptsTest', function() {
    return gulp.src(['src/www/js/*.js', '!src/www/js/_*.js'])
        .pipe(concat('script.min.js'))
        .pipe(gulp.dest('site/www/js'));
});

// Minify remaining JS
gulp.task('scriptsStatic', function() {
    return gulp.src(['src/www/js/_*.js'])
        .pipe(uglify())
        .pipe(rename(function (path) {
            path.basename = path.basename.substring(1) + ".min";
            return path;
        }))
        .pipe(gulp.dest('site/www/js'));
});

gulp.task('scriptsStaticTest', function() {
    return gulp.src(['src/www/js/_*.js'])
        .pipe(rename(function (path) {
            path.basename = path.basename.substring(1) + ".min";
            return path;
        }))
        .pipe(gulp.dest('site/www/js'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('src/**/*.*', gulp.series('static'));
    gulp.watch(['src/www/imgs/**/*.jpg', 'src/www/imgs/**/*.jpeg', 'src/www/imgs/**/*.png', 'src/www/imgs/**/*.gif'], gulp.series('images'));
    gulp.watch('src/**/*.js', gulp.series('lint', 'scripts', 'scriptsStatic'));
    gulp.watch('src/**/*.scss', gulp.series('sass'));
    gulp.watch('src/**/*.css', gulp.series('css'));
});

// Optimize images
gulp.task('images', function() {
    return gulp.src(['src/www/imgs/**/*.jpg', 'src/www/imgs/**/*.jpeg', 'src/www/imgs/**/*.png', 'src/www/imgs/**/*.gif'])
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
        ]))
        .pipe(gulp.dest('site'));
});

// Purge static cache folder
gulp.task('clearStaticCache', function(done) {
    var path = 'site/staticCache/*';
    del.sync(path);
    done();
});


// Default Task
gulp.task('test', gulp.parallel('static', 'lint', 'css', 'sass', 'scriptsTest', 'scriptsStaticTest', 'clearStaticCache'));
gulp.task('build', gulp.parallel('static', 'lint', 'css', 'sass', 'scripts', 'scriptsStatic'));
gulp.task('default', gulp.series('test', 'watch'));

gulp.task('clean', function() {
    return del.sync('site');
})
