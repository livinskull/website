<?php
    namespace dtw;

    class Medals {
        private static $medals;

        /**
        * Get array of all medals, unsorted
        *
        * @return array medals
        */
        public static function getMedals() {
            if (isset(static::$medals)) {
                return static::$medals;
            }

            $medals = \dtw\DtW::$redis->get('medals');
            if ($medals) {
                $medals = json_decode($medals);
            } else {
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `medal_id`
                    FROM `medals`
                ');
                $stmt->execute();
                if ($stmt->rowCount()) {
                    $medals = $stmt->fetchAll(\PDO::FETCH_COLUMN);

                    \dtw\DtW::$redis->set('medals', json_encode($medals));
                } else {
                    throw new \Exception('No medals found');
                }
            }

            // Load in medal details
            foreach($medals AS &$medal) {
                $medal = new Medal($medal);
            }

            static::$medals = $medals;

            return self::$medals;
        }


        /**
        * Get array of medals grouped and sorted. Each group contains array of medals
        *
        * @return array groups containing medal IDs
        */
        public static function getMedalsGrouped() {
            $medals = static::getMedals();

            $groups = Array();
            foreach($medals AS $medal) {
                if (array_key_exists($medal->label, $groups)) {
                    $group = $groups[$medal->label];
                } else {
                    $group = array();
                }

                array_push($group, $medal);

                $groups[$medal->label] = $group;
            }

            ksort($groups);

            return $groups;
        }

        // TODO
        public static function getMedal($medalID = null) {
            return new Medal($medalID);
        }

        public static function getUserMedal() {
            $medals = static::getMedals();

            // Remove medals not allowed in nav
            $medals = array_filter($medals, function($a) {
                return !!$a->order;
            });

            // Sort medals by order
            usort($medals, function($a, $b) {
                return ($a->order - $b->order);
            });

            foreach($medals AS $medal) {
                if (!$medal->hasCompleted() && $medal->getStepsTotal() > 1) {
                    $medal->progress = $medal->getMyProgress();
                    return $medal;
                }
            }
        }

        public static function checkUsersMedals($type = null, $userID = null) {
            $medals = static::getMedals();

            foreach($medals AS $medal) {
                if (!$type || $type == $medal->type || $medal->type == 'todo') {
                    if (!$userID) {
                        $medal->getMyProgress();
                    } else {
                        $medal->getUserProgress($userID);
                    }
                }
            }
        }

        public static function awardMedal($label = null, $colour = 'bronze', $userID = null) {
            $medals = static::getMedals();

            foreach($medals AS $medal) {
                if ($label == $medal->label && $colour == $medal->colour) {
                    $medal->awardMedal($userID);
                    break;
                }
            }
        }

        // API hooks
        public static function registerHooks() {
            // External API
            \dtw\DtW::$api->registerHook('medals', 'list', function($params) {
                return array_map(function($medal) {
                    $medal = self::getMedal($medal->id);
                    $data = $medal->getData();
                    $data->stats = $medal->getStats();

                    unset($data->type);
                    unset($data->count);
                    unset($data->order);

                    return $data;
                }, self::getMedals());
            }, true);


            \dtw\DtW::$api->registerHook('medals', 'medal', function($params) {
                $medal = self::getMedal($params['id']);
                $data = $medal->getData();
                $data->stats = $medal->getStats();

                unset($data->type);
                unset($data->count);
                unset($data->order);

                return $data;
            }, true);
        }

    }