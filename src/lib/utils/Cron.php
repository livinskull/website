<?php
    namespace dtw\utils;

    class Cron {
        private $tasks = array();

        public function __construct() {
            \dtw\DtW::$api->registerHook('cron', 'run', [$this, 'run']);
        }

        public function run($params) {
            if ($params['key'] !== \dtw\DtW::$config->get('api.key')) {
                return false;
            }

            $details = new \stdClass();

            define('CRON', true);
            $minutes = date('i');
            $details->completed = 0;

            try {
                if ($minutes % 15 === 0) {
                    $feed = new \dtw\utils\Feed();
                    $feed->updateNews();
                    $feed->updateExploits();
                    $details->completed++;
                }

                if ($minutes % 1 === 0) {
                    $discussions = new \dtw\Discussions();
                    $discussions->checkRecentPosts();
                    $details->completed++;

                    $discussions->saveViewCounts();
                    $details->completed++;

                    \dtw\Articles::saveViewCounts();
                    $details->completed++;

                    $details->emailsSent = \dtw\utils\Emailer::process();
                    $details->completed++;
                }
            } catch (\Exception $e) {
                print_r($e->getMessage());
                return false;
            }

            return array($details);
        }

    }
?>